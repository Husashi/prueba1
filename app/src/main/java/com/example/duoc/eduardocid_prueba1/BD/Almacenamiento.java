package com.example.duoc.eduardocid_prueba1.BD;

import com.example.duoc.eduardocid_prueba1.Usuario;

import java.util.ArrayList;

/**
 * Created by DUOC on 20-04-2017.
 */

public class Almacenamiento {
    private static ArrayList<Usuario> values = new ArrayList<>();
    public static void Poblar(){
        values.add(new Usuario("Husashi", "asdf1"));
        values.add(new Usuario("Feña", "asdf12"));
        values.add(new Usuario("Ian", "asdf123"));
        values.add(new Usuario("Soylen", "asdf1234"));
        values.add(new Usuario("Sebastian", "asdf12345"));
        values.add(new Usuario("Gabo", "asdf123456"));
    }

    public static void agregarUsuario(Usuario usuario){
        values.add(usuario);

    }

    public static ArrayList<Usuario> obtieneListadoUsuarios(){
        return values;
    }
}
