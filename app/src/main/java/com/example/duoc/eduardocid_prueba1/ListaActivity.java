package com.example.duoc.eduardocid_prueba1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.example.duoc.eduardocid_prueba1.BD.Almacenamiento;

import java.util.ArrayList;

public class ListaActivity extends AppCompatActivity {

    private ListView lvUsuario;
    private ArrayList<Usuario> dataSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);
        lvUsuario = (ListView) findViewById(R.id.lvUsuario);
        UsuarioAdapter adaptador = new UsuarioAdapter(this, getDataSource());
        lvUsuario.setAdapter(adaptador);

    }

    public ArrayList<Usuario> getDataSource() {
        if(dataSource == null){
            dataSource = Almacenamiento.obtieneListadoUsuarios();
        }
        return dataSource;
    }
}
