package com.example.duoc.eduardocid_prueba1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.duoc.eduardocid_prueba1.BD.Almacenamiento;

import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends AppCompatActivity {

    private EditText etUsuario, etClave;
    private Button btnEntrar, btnRegistrarse;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        if(Almacenamiento.obtieneListadoUsuarios().isEmpty())
        {
            Almacenamiento.Poblar();
        }

        etUsuario=(EditText)findViewById(R.id.etUsuario);
        etClave=(EditText)findViewById(R.id.etClave);
        btnEntrar=(Button)findViewById(R.id.btnEntrar);
        btnRegistrarse=(Button)findViewById(R.id.btnRegistrarse);
        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Usuario> u= Almacenamiento.obtieneListadoUsuarios();
                String mensajeError="";
                if(etUsuario.getText().toString().length()<1){
                    mensajeError+=" - El campo Usuario esta vacio";
                }
                if(etClave.getText().toString().length()<1){
                    mensajeError+=" - El campo Clave esta vacio";
                }

                if(etClave.getText().toString().length()>0 && etUsuario.getText().toString().length()>0){
                    for(Usuario r : u){
                        if(etUsuario.getText().toString().equals(r.getUsuario().toString()) && etClave.getText().toString().equals(r.getClave().toString())) {
                            Intent i = new Intent(LoginActivity.this, ListaActivity.class);
                            startActivity(i);
                            break;
                        }mensajeError = "Usuario o Clave incorrectos";
                    }

                }
                if(!mensajeError.equals("")){
                    Toast.makeText(LoginActivity.this, mensajeError, Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnRegistrarse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, RegistroActivity.class);
                startActivity(i);
            }
        });

    }
}
