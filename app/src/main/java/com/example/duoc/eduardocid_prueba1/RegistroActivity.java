package com.example.duoc.eduardocid_prueba1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.duoc.eduardocid_prueba1.BD.Almacenamiento;

public class RegistroActivity extends AppCompatActivity {

    private EditText etUsuario, etClave, etRClave;
    private Button btnCrear, btnVolver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        etUsuario=(EditText)findViewById(R.id.etUsuario);
        etClave=(EditText)findViewById(R.id.etClave);
        etRClave=(EditText)findViewById(R.id.etRClave);
        btnCrear=(Button)findViewById(R.id.btnCrear);
        btnVolver=(Button)findViewById(R.id.btnVolver);

        btnCrear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                String mensajeError="";
                if(etUsuario.getText().toString().length()<1){
                    mensajeError+=" - El campo Usuario esta vacio";
                }
                if(etClave.getText().toString().length()<1){
                    mensajeError+=" - El campo Clave esta vacio";
                }
                if(etRClave.getText().toString().length()<1){
                    mensajeError+=" - El campo Repetir Clave esta vacio";
                }
                if(!etClave.getText().toString().equals(etRClave.getText().toString())){
                    mensajeError+=" - Las claves ingresadas no coinciden";
                }
                if(mensajeError.equals("")){
                    Usuario u= new Usuario();
                    u.setUsuario(etUsuario.getText().toString());
                    u.setClave(etClave.getText().toString());
                    Almacenamiento.agregarUsuario(u);
                    Toast.makeText(RegistroActivity.this, "Usuario creado correctamente", Toast.LENGTH_SHORT).show();
                    Intent i =new Intent(RegistroActivity.this, LoginActivity.class);
                    startActivity(i);
                }
                else
                {
                    Toast.makeText(RegistroActivity.this, mensajeError, Toast.LENGTH_SHORT).show();
                }

            }
        });
        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }
}
