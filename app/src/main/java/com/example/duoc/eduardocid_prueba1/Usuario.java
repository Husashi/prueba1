package com.example.duoc.eduardocid_prueba1;

/**
 * Created by DUOC on 20-04-2017.
 */

public class Usuario {
    private String Usuario;
    private String clave;

    public Usuario() {}

    public Usuario(String usuario, String clave) {
        Usuario = usuario;
        this.clave = clave;
    }

    public String getUsuario() {
        return Usuario;
    }

    public void setUsuario(String usuario) {
        Usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }
}
